package com.example.include_sdk_flutter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.annotation.NonNull
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.android.FlutterActivity.*
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import kotlin.random.Random

class MainActivity : FlutterActivity() {

    lateinit var btn1: Button
    lateinit var btn2: Button
    lateinit var btn3: Button

//    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
//        super.configureFlutterEngine(flutterEngine)
//        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, "example.com/channel").setMethodCallHandler {
//                call, result ->
//            if(call.method == "getRandomString") {
//                val limit = call.argument("len") ?: 4
//                val prefix = call.argument("prefix") ?: ""
////                val rand = ('a'..'z').shuffled().take(4).joinToString("")
//                val rand = ('a'..'z')
//                    .shuffled()
//                    .take(limit)
//                    .joinToString(prefix = prefix, separator = "")
//                result.success(rand)
//            }
//            else {
//                result.notImplemented()
//            }
//        }
//    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn1 = findViewById(R.id.btn1)
//        btn2 = findViewById(R.id.btn2)
//        btn3 = findViewById(R.id.btn3)

        btn1.setOnClickListener {
            startActivity(
                FlutterActivity.createDefaultIntent(this)
            )

        }
//        btn2.setOnClickListener {
//            startActivity(
//                withNewEngine()
//                    .initialRoute("/r2")
//                    .build(this)
//            )
//
//        }
//        btn3.setOnClickListener {
//            startActivity(
//                withNewEngine()
//                    .initialRoute("/r3")
//                    .build(this)
//            )
//
//        }

    }
}