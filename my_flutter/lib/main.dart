import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_flutter/test_file.dart';
import 'package:my_flutter/test_file1.dart';
import 'package:my_flutter/test_file2.dart';
import 'package:pti_insurance_test/pti_insurance_test.dart';
import 'package:pti_insurance_test/utils/common.dart';

void main() => runApp(const MyApp());

// class MyApp extends StatelessWidget {
//   const MyApp({super.key});
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: const MyHomePage(title: 'Flutter Demo Home Page'),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   const MyHomePage({super.key, required this.title});
//   final String title;
//   @override
//   State<MyHomePage> createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   String _counter = '';
//   static const platform = MethodChannel('example.com/channel');
// Future<void> _generateRandomString() async {
//     String random = '';
//     try {
//        var arguments = {
//         'len': 5,
//         'prefix': 'anhnt_',
//       };
//       random = await platform.invokeMethod('getRandomString',arguments);
//       print(random.runtimeType);
//     } on PlatformException catch (e) {
//       random = '';
//     }
// setState(() {
//       _counter = random;
//     });
//   }
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(widget.title),
//       ),
//       body: Center(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             const Text(
//               'Kotlin generates the following string:',
//             ),
//             Text(
//               '$_counter',
//               style: Theme.of(context).textTheme.headline4,
//             ),
//           ],
//         ),
//       ),
//       floatingActionButton: FloatingActionButton(
//         onPressed: _generateRandomString,
//         tooltip: 'Generate',
//         child: const Icon(Icons.refresh),
//       ),
//     );
//   }
// }
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});


  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  String token =
      "A/YpuiBYLsD3mEwI604+ez3Gwr7Cz3krelOhvQTayBOSCzIjTDeGO21wJnMepVQTbSGAqeKGmlnDRBssr1RUR/negVNVZv5nAcZqVw7bK4MOhNPy5+GVoIRbxzdjkft9kfeY7uNOv4lERwyI7TIYsG8OhwYQM13ZKs4wzM+DmVfmatkPPpBNwcHe2fydh4hDUqHz1QaxdyMbHbnmBmNNHeTvvNjC69FdRuUVD9Gs27/xrwHYcHTSEVbq4Ui8ByV2y6Dy2QHrT4bXG4RdBjuDpbR0cowOcwDJ1JNVVjpSK/yryyWzrbQFZmlkPK/+Ltqj5uAuQBgjkbEELYLNGPNUEilAZJSgi5Sk/m3LHo4E9vREUdQP3Od3jrGOfAbmzbUlNNgxZaRhNukjPh9eZrnvx7BIBhVmJq0A4FRzAC76/2PRrqCpGB6zwwpQHfBl6t25zSNs7wwYJzYgDS5NXlpvP9NwztUYBA/+lpIuA90MMa5csji1nnwMMy/JPxTOzS3Xe9wvKSm0Eb/xHMrB3IDkz8gTarGWXpAVJqRGsiqptiWAiwPvpS3F8kYobs9ax2ueDFob88147qHr07JenEWE7PKPr9BeIhZtKvfCuC+JSahUWPuORk5YP64o0SMEkJVAiGNa40outDiOWCioH5B/t1LTcgTyU1mxzMSem2TCLtYpGoYpHaM9y2dRPCoz+B4BbBIjT9bUNIHiLXTmcJSd8NQeknrh6exjevWnecaFaCSY3SIFIeIUe5+rwZ2bHhS3rbCVC8As0+xXcZl6whuuCPHUe1L2YIsp0XscG8ojIAQ4GRjnkRbv+fHjClBk2cyXpwnvLSJwQU5zug1150b9TZV+1XaT2Nd/Glwg1fOFUHlUp+W78yKRTinFjwhwaUtsEfLFUQCLmHaC16s8KAfCpIbTHUiqYLlEmAKK3iTD+/32ojzBsfsfm7HUeVimb59kbdRanh2BFLH6UjhtUOlKTbljgL927ZQYY+v+zd/hm2H8DUhnjQi4h/ot6N0G82cX6AIuWWlju+qM9X63NlqVKETWkgXmXSfVYGHzZhp9SnF/bqHDBUoz0RMpUMgj9vI0+/Z/OGQ2a/Z9AFGe/3I3bNW8ZcLEGyRNZ01uto9m44Z9YzyAQYrUrv8x3w9RsvZmJKQjjp1xk7rWxeBk1wVfhQcdoqWzGyZKyjxhv70LobwJF94l58AVq/7ChAU6+BiiN+GJYSS+8FiW49gAWg987dZq7o0U9WQRmGv2hieRapEzip8gUGNNft8w3sBbo9K50GSNA5Wxabyq+TPeG/7elpCkmKu4J1/l2tjd4B94MccGmMhBY5CdwN63M3Z1C08qVAfSu1qiYKoBTupl+EcRd76WYEvOfHQr3ji+puiT/1wunIgWFVdJjn7TQ0E6+RSHviksGNwfjMKUIzxkohf2zBDEwFFFn0Rq5c4NTuU1K5vjfvQAxp2MiDFzGdXMDHvo09FcOlqTG3x5jGJmcuAjUrrzDo2heAQoN9k2Qz9qzbyivzFzsjiJjB5spAqY31VLgzE43wp+jszMNOD8SjfEFgoi6rHwiHVTwlpxqequy2rFvXGVdV+mTwY10VNkEWlk";
  void _incrementCounter() {
    // setState(() {
    //   // This call to setState tells the Flutter framework that something has
    //   // changed in this State, which causes it to rerun the build method below
    //   // so that the display can reflect the updated values. If we changed
    //   // _counter without calling setState(), then the build method would not be
    //   // called again, and so nothing would appear to happen.
    //   _counter++;
    // });
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => PtiView(
                token: token,
                kenh: "KTTT",
                maCN: "000",
                maNV: "XE",
                typeView: TypeView.vehicle,
                type: TypeFrom.createNew,
              )),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: const DemoView(),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}